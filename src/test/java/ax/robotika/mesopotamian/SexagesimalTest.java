package ax.robotika.mesopotamian;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SexagesimalTest {

    @Test
    void toInteger() {
      int base = 42;
      Sexagesimal num = new Sexagesimal(base);
      assertEquals(num.toInteger(), base, "the internal integer is supposed to be returned");
    }

    @Test
    void asSexagesimal() {
    }

    @Test
    void compareTo() {
      Sexagesimal num1 = new Sexagesimal(52);
      Sexagesimal num2 = new Sexagesimal(52);
      assertEquals(num1, num2, num1.toString() + " should equal " + num2.toString());
    }
}
