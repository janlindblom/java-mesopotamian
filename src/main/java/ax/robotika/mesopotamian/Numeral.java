package ax.robotika.mesopotamian;

import java.util.Map;

import static java.util.Map.entry;

public class Numeral {

  public static String numberToString(final Integer number) {
    Name numeral = numberNameMap.get(number);
    return nameSymbolMap.get(numeral);
  }

  private enum Name {
    Dish,
    Min,
    Esh,
    Limmu
  }

  private static final Map<Integer, Name> numberNameMap = Map.ofEntries(
    entry(1, Name.Dish),
    entry(2, Name.Min),
    entry(3, Name.Esh),
    entry(4, Name.Limmu)
  );

  private static final Map<Name, String> nameSymbolMap = Map.ofEntries(
    entry(Name.Dish, "𒁹"),
    entry(Name.Min, "𒈫"),
    entry(Name.Esh, "𒐈"),
    entry(Name.Limmu, "𒐉")
  );
}
