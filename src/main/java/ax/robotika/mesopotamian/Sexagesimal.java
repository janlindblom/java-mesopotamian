package ax.robotika.mesopotamian;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Sexagesimal implements Comparable<Sexagesimal> {
  private Integer num;

  public Sexagesimal(int num) {
    this.num = num;
  }

  public int toInteger() {
    return num;
  }

  public List<Integer> asSexagesimal() {
    ArrayList<Integer> arr = new ArrayList<>();
    int index = 0;
    int input = this.num;
    while (input != 0) {
      arr.add(0, input % 60);
      input /= 60;
    }
    return arr;
  }

  @Override
  public int compareTo(Sexagesimal o) {
    return this.num.compareTo(o.num);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Sexagesimal number = (Sexagesimal) o;
    return num.equals(number.num);
  }

  @Override
  public int hashCode() {
    return Objects.hash(num);
  }
}
